#!/bin/bash
sudo apt-get update 
sudo apt-get upgrade-y

curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner

sudo usermod -aG docker gitlab-runner

sudo gitlab-runner register         \
--non-interactive                   \
--url https://gitlab.com            \
--token SEU_TOKEN                   \
--executor shell                    \