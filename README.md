# Desafio Avanti Bootcamp DevOps

## Sobre o Desafio

Este desafio foi proposto com o objetivo de aplicar os conhecimentos adquiridos no bootcamp Avanti DevOps, criando uma infraestrutura com Terraform na AWS, Conteinerizando uma aplicação simples em Javascript e realizando deploy via GitLab CI/CD.

## Índice

1. [Pré-requisitos](#pré-requisitos)
2. [Estrutura do Projeto](#estrutura-do-projeto)
3. [Clonando o Repositório](#clonando-o-repositorio)
4. [Configurando Runner no GitLab](#configurando-runner-no-gitlab)
5. [Configurando Variáveis de Ambiente no GitLabb](#configurando-variaveis-de-ambiente-no-gitlab)
6. [Configuração do Terraform](#configuração-do-terraform)
7. [Configuração do Dockerfile](#configuração-do-dockerfile)
8. [Pipeline do GitLab CI/CD](#pipeline-do-gitlab-cicd)
9. [Executando Pipeline](#executando-pipeline)

## 1. Pré-requisitos

- Conta na AWS com permissões para criar instâncias EC2, grupos de segurança e outros recursos.
- AWS CLI instalado e configurado com suas credenciais.
- Terraform instalado em sua máquina local.
- Conta no GitLab para CI/CD.

## 2. Estrutura do Projeto

```plaintext
├── app
│   ├── app.js
│   ├── Dockerfile
│   ├── index.html
│   └── style.css
├── .gitlab-ci.yml
├── ec2.tf
├── main.tf
├── script.sh
├── security-group.tf
└── README.md
```

## 3. Clonando o Repositório
```sh
git clone https://gitlab.com/arthuredn02/avanti-atividade-1.git
cd seu-diretorio-repo
```

## 4. Configurando Runner no GitLab

### Criando Runner

1. **Acesse as Configurações do Projeto:**
   - No GitLab, navegue até o seu projeto.

2. **Configurar Variáveis de Ambiente:**
   - Vá para "Settings" e selecione "CI / CD" no menu lateral.
   - Selecione "Runners" para criar o runner do projeto.

3. **Configuração do Runner:**
   - Siga as instruções para configurar o runner conforme suas necessidades. Certifique-se de definir os seguintes detalhes durante a configuração:
     - Sistema operacional: Escola o de sua preferência
     - Executor: Escolha o executor Docker ou Shell.
     - Tags: Tags para identificar o runner.

## 5. Configurando Variáveis de Ambiente no GitLab

### Criando variáveis de ambiente

1. **Acessando as Configurações do Projeto:**
   - No GitLab, navegue até o seu projeto.

2. **Configurando Variáveis de Ambiente:**
   - Vá para "Settings" e selecione "CI / CD" no menu lateral.
   - Selecione "Variables" para adicionar novas variáveis de ambiente.

3. **Adicionando Variáveis:**
   - Adicione as seguintes variáveis de ambiente:
     - `DOCKERHUB_USERNAME`: Seu nome de usuário no Docker Hub.
     - `DOCKERHUB_PASSWORD`: Sua senha no Docker Hub.

   - Desmarque a opção "Protected" ao criar as variáveis para que elas possam ser acessadas pelo runner.

## 6. Configuração do Terraform

### Arquivos de configuração do Terraform

#### main.tf

O arquivo main.tf configura o provedor AWS e define a região.

#### ec2.tf
O arquivo ec2.tf configura a instância EC2.

#### security-groups.tf
O arquivo security-groups.tf configura os grupos de segurança.

#### script.sh
Este script bash automatiza o processo de instalação e configuração do GitLab Runner em um ambiente Linux. Abaixo estão as ações realizadas pelo script:

- Atualiza o sistema operacional.
- Instala o Docker.
- Adiciona o repositório do GitLab Runner e instala o GitLab Runner.
- Adiciona o usuário gitlab-runner ao grupo docker para acesso ao Docker.
- Registra o GitLab Runner com o GitLab, usando um token de registro e configurando-o para executar os jobs usando o executor Shell.

### Aplicação das configurações do Terraform

#### 1.Inicialize o Terraform no projeto:

```sh
terraform init
```

#### 2.Verifique as mudanças que serão realizadas:

```sh
terraform plan
```

#### 3.Aplique as configurações do Terraform:

```sh
terraform apply
```

## 7. Configuração do Dockerfile
O Dockerfile a seguir configura o ambiente para usar a imagem httpd como base e copia os arquivos da aplicação para o diretório raiz do servidor Apache, expondo a porta 80 para acesso externo.

1. **Criando o Dockerfile:**
   - Vá até o diretorio app e crie o Dockerfile.

2. **Configurando o Dockerfile:**
  
```Dockerfile
FROM httpd:latest
WORKDIR /usr/local/apache2/htdocs/
COPY . /usr/local/apache2/htdocs/
EXPOSE 80
```

## 8. Pipeline do GitLab CI/CD

O arquivo a segui define dois estágios: "build" e "deploy". No estágio "build", a imagem Docker é criada e enviada para o Docker Hub. No estágio "deploy", a imagem é baixada e executada em um contêiner Docker na porta 80.

1. **Criando o arquivo .gitlab-ci.yml:**
   - Vá até o diretorio raiz do projeto e crie o arquivo yml.

2. **Configurando o arquivo .gitlab-ci.yml:**
  
```yaml
stages:
  - build
  - deploy

variables:
  VERSION: "1.0"
  DOCKER_IMAGE_PATH: seu-usuario/nome-da-imagem:$VERSION

create_image:
  stage: build
  tags:
    - DOCKER
  script: 
    - docker login -u $user -p $password
    - docker build -t $DOCKER_IMAGE_PATH app/.
    - docker push $DOCKER_IMAGE_PATH

run_docker:
  stage: deploy
  tags:
    - DOCKER
  before_script:
    - if [ "$(docker ps -a -q -f name=nome-da-imagem)" ]; then
        docker rm app-docker --force;
      fi
    - if [ "$(docker images -q $DOCKER_IMAGE_PATH)" ]; then
        docker rmi $DOCKER_IMAGE_PATH --force;
      fi
  script: 
    - docker pull $DOCKER_IMAGE_PATH || exit 1
    - docker run -dti -p 80:80 --name nome-da-imgem $DOCKER_IMAGE_PATH || exit 1
```
## 9. Executando Pipeline

Depois que tudo estiver configurado, qualquer push para o repositório acionará o pipeline CI/CD, que construirá e implantará a aplicação.
